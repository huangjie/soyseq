Processing counts for transgenes
================================

Introduction
-------------
Reads were aligned against the entire soybean genome plus a "fake" chromosome consisting of three transgenes. 

Then, we used featureCounts to count the number of reads aligning to every soybean gene and the transgenes. I ran it three times with different options. Best read count inputs to use (I think) are the single-mapping "sm" read count files. Those files have "sm" in the name.

Prior to counting and aligning reads, I added transgenes to the annotation and genome files. Transgenes were added to the genome as part of a faux extra chromosome - named ChrT. (See data in this directory.) 

**Note**: When I ran featureCounts, I used an SAF file that did not use the best names for the transgenes. I will fix this in the code, but for now, just be aware that the transgene names might not be the same from file to file. 

Normalized RPM and RPKM files are in ../Counts/. The RPM an RPKM files use the correct (most user-friendly) transgene names. 

The samples come from three genotypes, three plants per genotype, and three soybeans per plant. Thus, we have 36 samples, twelve samples per genotype. 

Here, we analyze expression of the transgenes and ask: which ones were most highly expressed, and how much? We'd also like to know: How high was their expression relative to the rest of the genome? 

Questions:

* What was the epxression level (in RPKM) of the three transenes?
* What was the expression level relative to the rest of the annotated genes? Where they in the top 5%, the top 10%, the top 20%

For this analysis, we'll use the "sm" reads, because those reads were the most accurately mapped. 

Methods
-------

Read the RPKM data for the "sm", clean it up, reshape into a format we can use for data analysis.

```{r}
fname='../Counts/results/sm_RPKM.tsv.gz'
rpkm_sm=read.delim(fname,as.is=T)
```


Results
-------

ANSWER THE QUESTIONS

Session Information
-------------------

```{r}
sessionInfo()
```

