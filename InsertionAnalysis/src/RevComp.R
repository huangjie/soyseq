
# The sequences, as exported from snapgene, were reversed (left to right) from the dispaly in figure 1.
# The +/- strand distinction is arbitrary, and reversing it makes the figures face the same way.

suppressPackageStartupMessages(library("Biostrings"))

# read construct sequence from data/snapGene/[label]WholePlasmid.fa
# make reverse-complement
# write reverse-complemented sequence to [label]WholePlasmid.fa in
# current working directory
makeConstructFa=function(label) {
  construct=paste0(label,"WholePlasmid")
  fa=readDNAStringSet(paste0("data/snapGene/", construct, ".fa"), format="fasta")
  names(fa)=label
  faNew=reverseComplement(fa)
  fname=paste0(construct,'.fa')
  writeXStringSet(faNew, fname, format="fasta")
  return(fname)
}

# create fasta file Constructs.fa using reverse-complemented
# sequence for all three contructs, saves file in data
# Note: creates temporary files in current working directory and 
# then removes them
doAll=function(){
  fname="Constructs.fa"
  file.create(fname)
  labels=c("ST111","ST77","T764")
  for (label in labels) {
    faFileName=makeConstructFa(label)
    file.append(fname,faFileName)
    file.remove(faFileName)
  }
  system(paste('gzip -f',fname))
  newname=paste0(fname,'.gz')
  file.copy(newname,paste0('data/',newname),overwrite=T)
  file.remove(newname)
}

