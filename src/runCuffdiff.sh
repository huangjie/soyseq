#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l vmem=12000mb
#PBS -l walltime=20:00:00
cd $PBS_O_WORKDIR
WT=`ls WT*.bam | tr '\n' ',' | sed 's/.$//'`
SM=`ls $S*.bam | tr '\n' ',' | sed 's/.$//'`
CMD="cuffdiff -b $G -u -p 8 -o $O/$S -L $S,WT $T $SM $WT"
$CMD

# see https://groups.google.com/forum/#!topic/tuxedo-tools-users/xE5_c3AfDHo
# default max-bundle-frags is too big sometimes
