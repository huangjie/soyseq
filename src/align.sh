#!/bin/bash
# run like this: 
# align.sh 1>jobs.out 2>jobs.err
# then if you have to kill the jobs, 
# cat jobs.out | xargs qdel 
SCRIPT=runTopHat.sh
D=${1:-"soyseq_TH2.0.13"}
G=${2:-"G_max_Jan_2014_plusT"}
FS=`ls fastq/*.fastq.gz`
T=transcriptome_data/Gmax_275_Wm82.a2.v1.gene_exons
for F in $FS
do
    S=`basename $F .fastq.gz`
    CMD="qsub -N $S -o $S.out -e $S.err -vSAMPLE=$S,OUTDIR=$D,GENOME=$G,FASTQ=$F,TRANS=$T $SCRIPT"
    $CMD
done
