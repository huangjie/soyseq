#!/bin/bash
D=${1:-".."}
FILES=$(ls $D/*.bam)
for FILE in $FILES
do
    echo $FILE
    ln -s $FILE .
done