---
title: ""
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---
**Date run: `r date()`**

# Introduction


Since the 764 set has by far the most DE genes, we'll only make an output file for that comparison.  If we do any further analyses with the ST77 or ST111 results, there are few enough genes that we would just use the union to keep as many of them as possible.

# read data

## read annotation file
```{r}
source("../src/common.R")
#sets de_FDR
annots=readAnnots()
cd.col = "orange"
eR.col = "purple"
```

## read CuffDiff file
```{r}
cd.fname="../CuffAnalysis/originalDEgenesList/764_genes_exp.diff.gz"
cd=read.table(cd.fname, header=T, sep="\t", as.is=T)
genes=cd$gene
names(genes)=genes
genes=strsplit(genes, split=",")
genes=stack(genes)
names(genes)=c("gene", "multiGene")
cd=merge(genes, cd, by.x="multiGene", by.y="gene")
cd=merge(cd, annots, by="gene")
# cd still has duplicated genes, it isn't always clear why
# pick the best p_value, and remove the other dups
cd=cd[order(cd$p_value),]
cd=cd[!duplicated(cd$gene),]
row.names(cd)=cd$gene
```

Remove "Inf" and "-Inf".  Downstream programs will want a non-0 numerical value; "Inf" can cause problems.  The largest fold change reported by edgeR was about 10, so we'll set the Inf and -Inf values to 10 and -10.
```{r}
cd$log2.fold_change.[cd$log2.fold_change. == "Inf"] = 10
cd$log2.fold_change.[cd$log2.fold_change. == "-Inf"] = -10
# Flip the cuffdiff
cd$log2.fold_change. = -cd$log2.fold_change.
```

Put the cd data frame into a standard form that we will also with the edgeR data.
```{r}
cd = cd[,c("gene", "log2.fold_change.", "q_value", "value_2", "value_1")]
names(cd) = c("gene", "logFC", "fdr", "WT", "E764")
```


## read edgeR file
```{r}
eR.fname = "../DiffExp/results/T764.tsv.gz"
eR=read.delim(eR.fname,sep='\t',as.is=T,header=T)
row.names(eR)=eR$gene
eR  = eR[,c("gene", "logFC", "fdr", "WT", "E764")]
```


# Determine which entry makes the best case
For each gene, determine which pipelines values to use in the merged output.

```{r}
m = merge(cd, eR, by="gene", all=T) 
row.names(m)=m$gene
test.fdr = m$fdr.x
test.fdr[is.na(test.fdr)] = 1
test.q_value = m$fdr.y
test.q_value[is.na(test.q_value)] = 1
case1 = test.fdr <= test.q_value
case2 = test.fdr > test.q_value
# make a list of genes that each case applies to
eRGenes = m[case1,"gene"]
cdGenes = m[case2,"gene"]
```

# Combene Sets

Reduce the cd list to just the genes whose fdr values were better in cd than in edgeR.
```{r}
cd = cd[cdGenes,]
```

Reduce the eR list to just the genes whose fdr values were better in edgeR than in cuffdiff.
```{r}
eR = eR[eRGenes,]
```

Combene the sets.
```{r}
m = rbind(eR, cd)
m = merge(m, annots, by="gene")
```


# Write output file
Write the file to use with web tools that require logFC for each DE genes as a file.
```{r}
m.brief = m[m$fdr<=de_FDR, c("gene", "logFC")]
write.table(m.brief, file="results/UnionDE.764.forWeb.txt", quote=F, sep='\t', row.names=F)
```

Write the file to use with GOseq.
```{r}
write.table(m, file="results/UnionDE.764.txt", quote=grep("descr", names(m)), sep='\t', row.names=F)
```

