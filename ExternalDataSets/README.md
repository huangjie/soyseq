# External Data Sets

Files are from external data sources, as follows:

* * *

## G_max_Aug_2010.bed.gz

Bed file containing gene models from IGBQuickLoad data repository, obtained from http://www.igbquickload.org/quickload/G_max_Aug_2010/G_max_Aug_2010.bed.gz May 24, 2013. 

## G_max_Aug_2010_Jun_24_2013_update.bed.gz

Bed detail file containing gene models from IGBQuickLoad data repository, using updated gene models from Phytozome. Field 14 comes from annotations.tsv.gz. 

## G_max_Jan_2012.bed.gz

Annotations current as of Feb. 2015. 

## annotations.tsv.gz

Tab-separated file written by src/writeAnnotsFile.py. Maps gene names onto gene descriptions. Gene descriptions are from Glyma1.0-annotations.tsv, originally from Phytozome.

## Gmax_275_Wm82.a2.v1.annotation_info.txt.gz 

Downloaded from http://genome.jgi.doe.gov/Gmax/download/_JAMO/53112aed49607a1be0055a2b/Gmax_275_Wm82.a2.v1.annotation_info.txt?requestTime=1423178109 Feb. 5, 2015. 

## Gmax_275_Wm82.a2.v1.gene_exons.gff3.gz 

Downloaded from same location as above, Feb. 5, 2015.